# azrs-tracking
- This project for the 'Software Development Tools' course at the Faculty of Mathematics, University of Belgrade, addresses various software tools.
- The issues listed below provide details on the tools demonstrated in this project. 
    - [GDB](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/11)
    - [CMake](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/5)
    - [GCov](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/9)
    - [Gammaray](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/12)
    - [Clang Format](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/2)
    - [Clang Tidy](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/4)
    - [Valgrind](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/10)
    - [Docker](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/6)
    - [CI](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/8)
    - [Doxygen](https://gitlab.com/Subrutina17a/azrs-tracking/-/issues/3)
